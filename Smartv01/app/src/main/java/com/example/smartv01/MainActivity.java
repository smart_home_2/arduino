package com.example.smartv01;

import androidx.appcompat.app.AppCompatActivity;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "bluetooth1";


private static String address = "20:15:11:23:15:72";  //Вместо “00:00” Нужно нудет ввести MAC нашего bluetooth
    private static final int REQUEST_ENABLE_BT = 1;
    private BluetoothAdapter btAdapter = null;
    private BluetoothSocket btSocket = null;
    private OutputStream outStream = null;
    private InputStream mmInStream = null;
    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    MyTask mt;
    TextView textname;
    TextView textname2;
    TextView textname3;
    private StringBuilder sb = new StringBuilder();
    ThreadConnected myThreadConnected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //HttpClient client = new DefaultHttpClient();
        //HttpPost post = new HttpPost(address);
        textname = (TextView) findViewById(R.id.textView);
        textname2 = (TextView) findViewById(R.id.textView2);
        textname3 = (TextView) findViewById(R.id.textView3);
        Button button1 = (Button) findViewById(R.id.button); //Добавляем сюда имена наших кнопок
        Button button2 = (Button) findViewById(R.id.button2);
        btAdapter = BluetoothAdapter.getDefaultAdapter();
        checkBTState();
        button1.setOnClickListener(new View.OnClickListener()  //Если будет нажата кнопка 1 то
        {
            public void onClick(View v)
            {
                sendData("1");// Посылаем цифру 1 по bluetooth
                try {
                    String URL=getUrl("1");
                    Toast.makeText(getBaseContext(), URL, Toast.LENGTH_SHORT).show();
                    textname3.setText("Свет включен");
                    mt = new MyTask();
                    mt.execute(URL);
                } catch (Exception e) {
                    errorExit("Fatal Error"," On URL "+e.getMessage()+".");
                    e.printStackTrace();
                }
                Toast.makeText(getBaseContext(), "Включаем LED", Toast.LENGTH_SHORT).show();  //выводим на устройстве сообщение
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {
                sendData("0"); // Посылаем цифру 1 по bluetooth
                try {
                    String URL=getUrl("0");
                    textname3.setText("Свет выключен");
                    mt = new MyTask();
                    mt.execute(URL);
                } catch (UnsupportedEncodingException e) {
                    errorExit("Fatal Error"," On URL "+e.getMessage()+".");

                }
                Toast.makeText(getBaseContext(), "Выключаем LED", Toast.LENGTH_SHORT).show();
            }
        });

    }
    public void OnClic(View view){
        textname.setText("Молодец");
    }
    @Override
    public void onResume() {
        super.onResume();
        try {
            Log.d(TAG, "...onResume - попытка соединения...");
            // Set up a pointer to the remote node using it's address.
            BluetoothDevice device = btAdapter.getRemoteDevice(address);
            // Two things are needed to make a connection:
            //   A MAC address, which we got above.
            //   A Service ID or UUID.  In this case we are using the
            //     UUID for SPP.
            try {
                btSocket = device.createRfcommSocketToServiceRecord(MY_UUID);
            } catch (IOException e) {
                errorExit("Fatal Error", "In onResume() and socket create failed: " + e.getMessage() + ".");
            }
            // Discovery is resource intensive.  Make sure it isn't going on
            // when you attempt to connect and pass your message.
            btAdapter.cancelDiscovery();
// Establish the connection.  This will block until it connects.


            Log.d(TAG, "...Соединяемся...");
            try {
                btSocket.connect();
                myThreadConnected = new ThreadConnected(btSocket);
                myThreadConnected.start(); // запуск потока приёма и отправки данных
                Log.d(TAG, "...Соединение установлено и готово к передачи данных...");
            } catch (IOException e) {
                try {
                    btSocket.close();
                } catch (IOException e2) {
                    errorExit("Fatal Error", "In onResume() and unable to close socket during connection failure" + e2.getMessage() + ".");
                }
            }
            Log.d(TAG, "...Создание Socket...");

            try {
                outStream = btSocket.getOutputStream();
            } catch (IOException e) {
                errorExit("Fatal Error", "In onResume() and output stream creation failed:" + e.getMessage() + ".");
            }
        }catch (Exception ew) {
            errorExit("Fatal Error", "In onResume() and output stream creation failed:" + ew.getMessage() + ".");
        }

    }
    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "...In onPause()...");

        if (outStream != null) {
            try {
                outStream.flush();
            } catch (IOException e) {
                errorExit("Fatal Error", "In onPause() and failed to flush output stream: " + e.getMessage() + ".");
            }
        }
        try     {
            btSocket.close();
        } catch (IOException e2) {
            errorExit("Fatal Error", "In onPause() and failed to close socket." + e2.getMessage() + ".");
        }
    }
    private void checkBTState() {
        // Check for Bluetooth support and then check to make sure it is turned on
        // Emulator doesn't support Bluetooth and will return null
        if(btAdapter==null) {
            errorExit("Fatal Error", "Bluetooth не поддерживается");
        } else {
            if (btAdapter.isEnabled()) {
                Log.d(TAG, "...Bluetooth включен...");
            } else {
                //Prompt user to turn on Bluetooth
                Intent enableBtIntent = new Intent(btAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
        }
    }
    private void errorExit(String title, String message){
      Toast.makeText(getBaseContext(), title + " - " + message, Toast.LENGTH_LONG).show();
      finish();
   }
    private void sendData(String message) {
       byte[] msgBuffer = message.getBytes();

       Log.d(TAG, "...Посылаем данные: " + message + "...");

       try {
           outStream.write(msgBuffer);
       } catch (IOException e) {
           String msg = "In onResume() and an exception occurred during write: " + e.getMessage();
           if (address.equals("00:00:00:00:00:00"))
               msg = msg + ".\n\nВ переменной address у вас прописан 00:00:00:00:00:00, вам необходимо прописать реальный MAC-адрес Bluetooth модуля";
           msg = msg +  ".\n\nПроверьте поддержку SPP UUID: " + MY_UUID.toString() + " на Bluetooth модуле, к которому вы подключаетесь.\n\n";

           errorExit("Fatal Error", msg);
       }
   }
   private String getUrl(String status) throws UnsupportedEncodingException {
       String server = "http://3pivlsmhm.u0588704.plsk.regruhosting.ru/api/Arduino/";
       String params = "status="+URLEncoder.encode(status,"UTF-8")+
               "&login="+URLEncoder.encode("Arduino","UTF-8");
       return server+"?"+params;
   }
   private String getLogin(){
        return "Arduino";
   }
    class MyTask extends AsyncTask<String, Integer, String> {
        private Exception exception;


        @Override
        protected String doInBackground(String... arg) {
            try {
                URL obj = new URL(arg[0]);
                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("GET");
                InputStreamReader stream = new InputStreamReader(con.getInputStream());
                BufferedReader in = new BufferedReader(stream);
                String tmp;
                StringBuffer response = new StringBuffer();
                while ((tmp = in.readLine()) != null) {
                    response.append(tmp);
                }
                in.close();
                stream.close();
            }catch (IOException e){

            }
            return null;
        }

    }



    private class ThreadConnected extends Thread {    // Поток - приём и отправка данных

        private final InputStream connectedInputStream;
        private final OutputStream connectedOutputStream;

        private String sbprint;

        public ThreadConnected(BluetoothSocket socket) {

            InputStream in = null;
            OutputStream out = null;

            try {
                in = socket.getInputStream();
                out = socket.getOutputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }

            connectedInputStream = in;
            connectedOutputStream = out;
        }


        @Override
        public void run() { // Приём данных

            while (true) {
                try {
                    byte[] buffer = new byte[1];
                    int bytes = connectedInputStream.read(buffer);
                    String strIncom = new String(buffer, 0, bytes);
                    sb.append(strIncom); // собираем символы в строку
                    int endOfLineIndex = sb.indexOf("\r\n"); // определяем конец строки

                    if (endOfLineIndex > 0) {

                        sbprint = sb.substring(0, endOfLineIndex);
                        sb.delete(0, sb.length());

                        runOnUiThread(new Runnable() { // Вывод данных

                            @Override
                            public void run() {

                                switch (sbprint) {

                                    case "1":
                                        try {
                                            String URL=getUrl("1");
                                            Toast.makeText(getBaseContext(), URL, Toast.LENGTH_SHORT).show();
                                            textname3.setText("Свет включен");
                                            mt = new MyTask();
                                            mt.execute(URL);
                                        } catch (Exception e) {
                                            errorExit("Fatal Error"," On Arduino request 1 "+e.getMessage()+".");
                                            e.printStackTrace();
                                        }
                                        break;

                                    case "0":
                                        try {
                                            String URL=getUrl("0");
                                            Toast.makeText(getBaseContext(), URL, Toast.LENGTH_SHORT).show();
                                            textname3.setText("Свет выключен");
                                            mt = new MyTask();
                                            mt.execute(URL);
                                        } catch (Exception e) {
                                            errorExit("Fatal Error"," On Aruino request 0 "+e.getMessage()+".");
                                            e.printStackTrace();
                                        }
                                        break;

                                    default:
                                        break;
                                }
                            }
                        });
                    }
                } catch (IOException e) {
                    break;
                }
            }
        }
    }
}
